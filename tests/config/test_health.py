import django
from rest_framework import status
from rest_framework.test import APIClient

import pytest


django.setup()
_URL = "/health/"


@pytest.mark.django_db
def test_health_should_succeed(client: APIClient) -> None:
    response = client.get(_URL)
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {"health": "OK"}
