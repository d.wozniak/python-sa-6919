import pytest
from _pytest.capture import CaptureFixture, CaptureResult

from config.celery import clean_django_defender_data


@pytest.mark.django_db
def test_clean_django_defender_data(capsys: CaptureFixture) -> None:
    clean_django_defender_data()
    captured = capsys.readouterr()
    assert captured == CaptureResult(
        out="Starting clean up of django-defender table\n"
        "Finished. Removed 0 AccessAttempt entries.\n",
        err="",
    )
