from typing import Dict

import django
from django.contrib.auth.models import AbstractUser
from rest_framework import status
from rest_framework.test import APIClient

import pytest
from defender import utils as defender_utils
from defender.config import FAILURE_LIMIT

from tests.utils import TestAPIClient


django.setup()

_TEST_IP_ADDRESS: str = "127.0.0.1"
_EXPECTED_MESSAGE: Dict = {
    "detail": "The account has been locked for 300 seconds"
    " because you've failed to login 3 times."
}


@pytest.mark.django_db
def test_defender_locked_ip_should_fail(active_user: AbstractUser) -> None:
    defender_utils.block_ip(_TEST_IP_ADDRESS)
    client = TestAPIClient()
    response = client.authenticate(user=active_user)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.json() == _EXPECTED_MESSAGE
    defender_utils.unblock_ip(_TEST_IP_ADDRESS)


@pytest.mark.django_db
def test_defender_above_limit_login_attempts_should_fail(
    locked_user: AbstractUser, client: APIClient
) -> None:
    client = TestAPIClient()
    for _ in range(FAILURE_LIMIT + 1):
        response = client.authenticate(user=locked_user)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.json() == _EXPECTED_MESSAGE
    defender_utils.unblock_username(locked_user.username)
    defender_utils.unblock_ip(_TEST_IP_ADDRESS)
