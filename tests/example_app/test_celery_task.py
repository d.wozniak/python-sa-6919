import pytest

from example_app.tasks import celery_integration_test_task


@pytest.mark.django_db
def test_celery_integration_task_working() -> None:
    with pytest.raises(ZeroDivisionError):
        celery_integration_test_task().get(timeout=1)
