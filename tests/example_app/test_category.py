import django
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

import pytest
from faker import Faker

from example_app.models import Category
from tests.utils import TestAPIClient


django.setup()
_URL = reverse("example_app:category-list-create")


@pytest.mark.django_db
def test_category_list_without_login_should_fail(client: APIClient) -> None:
    response = client.get(_URL)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.json() == {
        "detail": "Authentication credentials were not provided."
    }


@pytest.mark.django_db
def test_list_locked_user_should_fail(client_locked_user: TestAPIClient) -> None:
    response = client_locked_user.get(_URL)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.json() == {
        "code": "token_not_valid",
        "detail": "Given token not valid for any token type",
        "messages": [
            {
                "message": "Token is invalid or expired",
                "token_class": "AccessToken",
                "token_type": "access",
            }
        ],
    }


@pytest.mark.django_db
def test_zero_subjects_should_return_empty_list(
    client_active_user: TestAPIClient,
) -> None:
    response = client_active_user.get(_URL)
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == []


@pytest.mark.django_db
def test_one_subject_exist_should_succeed(
    client_active_user: TestAPIClient, category: Category
) -> None:
    response = client_active_user.get(_URL)
    response_content = response.json()[0]
    assert response.status_code == status.HTTP_200_OK
    assert response_content.get("name") == category.name


@pytest.mark.django_db
def test_create_category_without_arguments_should_fail(
    client_active_user: TestAPIClient,
) -> None:
    response = client_active_user.post(_URL)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert "This field is required." in str(response.content)


@pytest.mark.django_db
def test_create_category_should_succeed(
    client_active_user: TestAPIClient,
) -> None:
    category_name = "Test"
    response = client_active_user.post(
        path=_URL,
        data={"name": category_name},
    )
    response_content = response.json()
    assert response.status_code == status.HTTP_201_CREATED
    assert response_content.get("name") == category_name


@pytest.mark.django_db
def test_create_category_with_too_long_name_should_fail(
    client_active_user: TestAPIClient,
) -> None:
    faker = Faker()
    category_name = faker.pystr(min_chars=33, max_chars=33)
    response = client_active_user.post(
        path=_URL,
        data={"name": category_name},
    )
    response_content = response.json()
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response_content == {
        "name": ["Ensure this field has no more than 32 characters."]
    }
