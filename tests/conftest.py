from typing import Generator

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser

import pytest

from example_app.models import Category
from tests.utils import TestAPIClient


USERNAME: str = "user"
PASSWORD: str = "secretkey"
USER = get_user_model()


@pytest.fixture
def active_user() -> Generator[AbstractUser, None, None]:
    user = _create_user()
    yield user
    user.delete()


@pytest.fixture
def locked_user() -> Generator[AbstractUser, None, None]:
    user = _create_user(is_active=False)
    yield user
    user.delete()


@pytest.fixture
def category() -> Generator[Category, None, None]:
    category = Category.objects.create(name="Higher")
    yield category
    category.delete()


@pytest.fixture
def client_active_user(
    active_user: AbstractUser,
) -> Generator[TestAPIClient, None, None]:
    client = TestAPIClient()
    client.authenticate(user=active_user)
    yield client
    client.logout()


@pytest.fixture
def client_locked_user(
    locked_user: AbstractUser,
) -> Generator[TestAPIClient, None, None]:
    client = TestAPIClient()
    client.authenticate(user=locked_user)
    yield client
    client.logout()


def _create_user(is_active: bool = True) -> AbstractUser:
    user, created = USER.objects.get_or_create(
        username=USERNAME,
    )
    if created:
        user.set_password(PASSWORD),
        user.is_active = is_active
        user.save()
    return user
