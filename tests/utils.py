from typing import Optional

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from rest_framework.response import Response
from rest_framework.test import APIClient


PASSWORD: str = "secretkey"


User = get_user_model()


class TestAPIClient(APIClient):
    _HEADER_TEMPLATE = "JWT {}"
    _DEFAULT_TOKEN_FIELD = "access"
    PATH_AUTH_JWT_LOGIN = "/v1/auth/jwt/create/"

    def authenticate(
        self, user: AbstractUser, path: str = PATH_AUTH_JWT_LOGIN
    ) -> Response:
        response = self.post(
            path=path,
            data={
                "username": getattr(user, User.USERNAME_FIELD),
                "password": PASSWORD,
            },
        )
        self._update_jwt_from_response(response)
        return response

    def _update_jwt_from_response(self, response: Response):
        jwt_token = self._get_token_from_response(response)
        self.credentials(HTTP_AUTHORIZATION=self._HEADER_TEMPLATE.format(jwt_token))

    @classmethod
    def _get_token_from_response(
        cls, response: Response, token_field: str = _DEFAULT_TOKEN_FIELD
    ) -> Optional[str]:
        return response.data.get(token_field)
