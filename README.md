# rq2877270

Application is for saving information about favourite pokemons for a particular user.
Think of it as a standalone service.

We use want to use 3rd party API for listing pokemons but we don't want to fetch it always directly from the integration. We want responses to be fast but at the same time we're not the owner of the data (list of pokemons).


In the main branch there is some base structure for the application.
Our developer was asked to create pull requests for two parts of the functionality.

First one is PR #1 with acctual integration with external API.
Second one PR #2 contains code to save information about favourite pokemons for a user.
