from rest_framework import routers


from pokemon.views import PokemonView


app_name = "pokemon"

pokemon = routers.DefaultRouter()
pokemon.register(r"pokemons", PokemonView)


urlpatterns = pokemon.urls
