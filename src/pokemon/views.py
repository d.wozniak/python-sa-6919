from rest_framework.viewsets import ModelViewSet

from .models import Pokemon
from pokemon.serializers import PokemonSerializer


class PokemonView(ModelViewSet):
    queryset = Pokemon.objects.all()
    serializer_class = PokemonSerializer
