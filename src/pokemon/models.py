from uuid import uuid4
from django.db import models


class Pokemon(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    name = models.CharField(max_length=32)
    size = models.IntegerField()
    external_id = models.IntegerField()

    def __str__(self):
        return f"Pokemon (id={self.id}, name={self.name}, size={self.size})"
