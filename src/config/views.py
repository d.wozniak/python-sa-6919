from rest_framework.request import Request
from rest_framework.response import Response

from rest_framework_simplejwt.views import TokenObtainPairView

from config.decorators import defended


class DefendedTokenObtainPairView(TokenObtainPairView):
    @defended
    def post(self, request: Request, *args, **kwargs) -> Response:
        return super().post(request=request, *args, **kwargs)
