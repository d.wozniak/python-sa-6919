import functools

from rest_framework.exceptions import AuthenticationFailed
from rest_framework.request import Request
from rest_framework.views import APIView

from defender import config
from defender.utils import add_login_attempt_to_db, check_request, is_already_locked

from config.exceptions import AccountLocked


def defended(method):
    def _get_username_from_request(request: Request) -> str:
        return request.data[config.USERNAME_FORM_FIELD]

    @functools.wraps(method)
    def _view_wrapper(self: APIView, request: Request, *args, **kwargs):
        if is_already_locked(request=request, get_username=_get_username_from_request):
            raise AccountLocked()
        result = None
        exception = None
        try:
            result = method(self, request, *args, **kwargs)
        except AuthenticationFailed as e:
            exception = e
        add_login_attempt_to_db(
            request=request,
            login_valid=exception is None,
            get_username=_get_username_from_request,
        )
        if not check_request(
            request=request,
            login_unsuccessful=exception is not None,
            get_username=_get_username_from_request,
        ):
            raise AccountLocked()
        if exception is not None:
            raise exception
        return result

    return _view_wrapper
