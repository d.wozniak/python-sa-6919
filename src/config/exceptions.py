from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import AuthenticationFailed

from defender.config import COOLOFF_TIME, FAILURE_LIMIT


class AccountLocked(AuthenticationFailed):
    _MESSAGE = (
        "The account has been locked for {time} seconds "
        "because you've failed to login {attempts} times."
    )

    def __init__(self):
        super().__init__(
            _(self._MESSAGE.format(time=COOLOFF_TIME, attempts=FAILURE_LIMIT))
        )
