from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles import views
from django.urls import include, path, re_path
from django.views.static import serve
from rest_framework_simplejwt.views import TokenRefreshView, TokenVerifyView

from config.views import DefendedTokenObtainPairView

urlpatterns = [
    path("admin/defender/", include("defender.urls")),
    path("admin/", admin.site.urls),
    path("auth/", include("djoser.urls")),
    re_path(
        r"^auth/jwt/create/?", DefendedTokenObtainPairView.as_view(), name="jwt-create"
    ),
    re_path(r"^auth/jwt/refresh/?", TokenRefreshView.as_view(), name="jwt-refresh"),
    re_path(r"^auth/jwt/verify/?", TokenVerifyView.as_view(), name="jwt-verify"),
    path("", include("pokemon.urls")),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        path(r"__debug__/", include(debug_toolbar.urls)),
        re_path(r"^static/(?P<path>.*)$", views.serve),
        re_path(r"^media/(?P<path>.*)$", serve, {"document_root": settings.MEDIA_ROOT}),
    ]

    from drf_spectacular.views import (
        SpectacularAPIView,
        SpectacularRedocView,
        SpectacularSwaggerView,
    )

    urlpatterns += [
        path("docs/", SpectacularSwaggerView.as_view(), name="swagger-ui"),
        path("docs/redoc/", SpectacularRedocView.as_view(), name="redoc"),
        path("schema/", SpectacularAPIView.as_view(), name="schema"),
    ]
