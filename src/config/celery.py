import os

from celery import Celery

# set the default Django settings module for the 'celery' program.
from src.config import settings


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")

app = Celery("config")

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object("django.conf:settings", namespace="CELERY")

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(
        settings.DEFENDER_DATA_CLEANUP_TIMESPAN_IN_SECONDS,
        clean_django_defender_data,
        name="Clean django defender data",
    )


@app.task
def clean_django_defender_data():
    from defender.management.commands.cleanup_django_defender import Command

    Command().handle()
